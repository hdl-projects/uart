library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_uartRec is
end tb_uartRec;

architecture tb of tb_uartRec is

    component uartRec
        port (clk     : in std_logic;
              rst     : in std_logic;
              rx      : in std_logic;
              recData : out std_logic_vector (7 downto 0);
              newData : out std_logic);
    end component;

    component uartTX
    port (clk       : in std_logic;
          rst       : in std_logic;
          data2send : in std_logic_vector (7 downto 0);
          send      : in std_logic;
          tx        : out std_logic;
          done      : out std_logic);
    end component;

    signal clk     : std_logic;
    signal rst     : std_logic;
    -- Signal for RX UART Unit
    signal recData : std_logic_vector (7 downto 0);
    signal newData : std_logic;
    -- Signal for TX UART Unit
    signal data2send : std_logic_vector (7 downto 0);
    signal send      : std_logic;
    signal tx        : std_logic;
    signal done      : std_logic;

    constant TbPeriod : time := 20 ns;  
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    txUnit : uartTX
    port map (clk       => clk,
              rst       => rst,
              data2send => data2send,
              send      => send,
              tx        => tx,
              done      => done);

    dut : uartRec
    port map (clk     => clk,
              rst     => rst,
              rx      => tx,
              recData => recData,
              newData => newData);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        rst <= '0';
        send <= '0';
        wait for 1 * TbPeriod;
        rst <= '1';
        send <= '1';
        data2send <="00000001";
        wait until newData='1';


    

        wait for 5 * TbPeriod;
        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_uartRec of tb_uartRec is
    for tb
    end for;
end cfg_tb_uartRec;