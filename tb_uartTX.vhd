----------------------------------------------
-- Design Name: Test Bench for UART TX 
-- Author: Pietro Pennestri
-- mail: pietro.pennestri@gmail.com
-- website: https://pennestri.me
-----------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity tb_uartTX is
end tb_uartTX;

architecture tb of tb_uartTX is

    component uartTX
        port (clk       : in std_logic;
              rst       : in std_logic;
              data2send : in std_logic_vector (7 downto 0);
              send      : in std_logic;
              tx        : out std_logic;
              done      : out std_logic);
    end component;

    signal clk       : std_logic;
    signal rst       : std_logic;
    signal data2send : std_logic_vector (7 downto 0);
    signal send      : std_logic;
    signal tx        : std_logic;
    signal done      : std_logic;

    constant TbPeriod : time := 20 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : uartTX
    port map (clk       => clk,
              rst       => rst,
              data2send => data2send,
              send      => send,
              tx        => tx,
              done      => done);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        data2send <= "10010001";
        send <= '0';
        rst <= '0';
        wait for 1*TbPeriod;
        
        rst  <= '1';
        send <= '1';

        -- EDIT Add stimuli here
        wait until done='1';

        wait for 5*TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_uartTX of tb_uartTX is
    for tb
    end for;
end cfg_tb_uartTX;